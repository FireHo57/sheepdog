﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Scriptables/Bot")]
public class Bot : ScriptableObject
{
    public int armor = 75;
    public int damage = 25;
    public GameObject gun;

    void Update()
    {
        // Update logic here...
    }
}
