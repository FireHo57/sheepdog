﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decisions/Look")]
public class LookDecision : Decision
{
    public string SearchTag;
    public float LookRadius = 0.0f; // the radius of the cyclinder that is cast
    public float LookDistance = 0.0f; // how far the cylinder is cast out to
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    private bool Look(StateController controller)
    {
        Transform relativeTransform = controller.transform.parent;

        Debug.DrawLine(relativeTransform.position, relativeTransform.position + relativeTransform.forward * LookDistance, Color.red);
        RaycastHit hit;
        if (Physics.SphereCast(relativeTransform.position, LookRadius, relativeTransform.position + relativeTransform.forward, out hit, LookDistance))
        {
            if (hit.collider.CompareTag(SearchTag))
            {
                controller.context[0] = hit.transform;
                return true;
            }
        }

        return false;
    }

}
