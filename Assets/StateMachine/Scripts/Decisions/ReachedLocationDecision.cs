﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decisions/ReachedDestinationDecision")]
public class ReachedLocationDecision : Decision
{
    public float SquaredAccuracy = 25.0f;

    public override bool Decide(StateController controller)
    {
        bool reachedLocation = CheckPosition(controller);
        return reachedLocation;
    }

    bool CheckPosition(StateController controller)
    {
        Transform context = controller.context[0];
        if( !context )
        {
            return false;
        }

        Vector3 distance = context.position - controller.transform.position;
        return (distance.sqrMagnitude < SquaredAccuracy - 0.0001f);
    }
}
