﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decisions/RandomDecision")]
public class RandomDecision : Decision
{
    public float chance = 0.5f;

    public override bool Decide(StateController controller)
    {
        return Random.value >= chance;
    }


}
