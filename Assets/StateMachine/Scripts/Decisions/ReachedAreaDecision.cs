﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/ReachedAreaDecision")]
public class ReachedAreaDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        bool reachedLocation = CheckPosition(controller);
        return reachedLocation;
    }

    bool CheckPosition(StateController controller)
    {
        Transform context = controller.context[0];
        if (!context)
        {
            return false;
        }

        // this decision requires a collider
        Collider contextCollider = context.GetComponent<Collider>();
        if(!contextCollider)
        {
            return false;
        }

        Collider[] hitcolliders = Physics.OverlapSphere(controller.transform.position, 0.1f);
        foreach( var collider in hitcolliders)
        {
            if(collider.transform == context)
            {
                return true;
            }
        }
        return false;
    }
}
