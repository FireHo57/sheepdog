﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/LeftAreaDecision")]
public class LeftAreaDecision : Decision
{
    public float checkRadius;
    public override bool Decide(StateController controller)
    {
        bool leftArea = LeftArea(controller);
        return leftArea;
    }

    bool LeftArea(StateController controller)
    {
        Collider[] hits = Physics.OverlapSphere(controller.transform.position, checkRadius);
        foreach(var hit in hits)
        {
            if(hit.transform == controller.context[0])
            {
                return false;
            }
        }

        return true;
    }
}
