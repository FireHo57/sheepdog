﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/RunOutOfFoodDecision")]
public class RunOutOfFoodDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool foodLeft = CheckFood(controller);
        return !foodLeft;
    }

    bool CheckFood(StateController controller)
    {
        if( controller.context[0] )
        {
            Eatable eatable = controller.context[0].GetComponent<Eatable>();
            if(eatable != null)
            {
                return eatable.CanBeEaten();
            }
        }

        return false;
    }
}
