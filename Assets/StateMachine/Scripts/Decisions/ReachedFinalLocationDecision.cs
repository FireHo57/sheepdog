﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/ReachedFinalLocationDecision")]
public class ReachedFinalLocationDecision : Decision
{
    public float SquaredAccuracy = 25.0f;

    public override bool Decide(StateController controller)
    {
        bool reachedLocation = CheckPosition(controller);
        if(reachedLocation)
        {
            controller.context.RemoveAt(0);
        }

        return reachedLocation;
    }

    bool CheckPosition(StateController controller)
    {
        Transform context = controller.context[controller.context.Count-1];
        if( !context )
        {
            return false;
        }

        Vector3 distance = context.position - controller.transform.position;
        return (distance.sqrMagnitude < SquaredAccuracy - 0.0001f);
    }
}
