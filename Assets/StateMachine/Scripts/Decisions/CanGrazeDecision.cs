﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decisions/CanGrazeDecision")]
public class CanGrazeDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return CanGraze(controller);
    }

    bool CanGraze(StateController controller)
    {
        Eatable eatable = controller.context[0].GetComponent<Eatable>();
        IEater eater = controller.agent.EatingStrategy;
        return eatable.EnoughToEat(controller.gameObject, eater);
    }
}
