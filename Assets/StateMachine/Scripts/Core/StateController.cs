﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour
{
    public State currentState;
    public State remainState;

    [HideInInspector] public FlockBehaviour currentBehaviour = null;
    public List<Transform> context;

    [HideInInspector] public Sheeple agent;

    // Start is called before the first frame update
    void Start()
    {
        currentBehaviour = currentState.behaviour;
        agent = GetComponentInParent<Sheeple>();
        context = new List<Transform>();

        if (currentState!= null)
        {
            currentState.OnEnter(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // AI always active, can add private bool if want to be able to disable it
        currentState.UpdateState(this);

    }

    public void TransitionToState(State nextState)
    {
        if(nextState!= remainState)
        {
            Debug.Log("Changing to state: "+nextState);
            currentState.OnExit(this);
            currentState = nextState;
            currentState.OnEnter(this);
            currentBehaviour = currentState.behaviour;

            agent.GetComponent<Renderer>().material.color = currentState.stateColour;
        }
    }
}
