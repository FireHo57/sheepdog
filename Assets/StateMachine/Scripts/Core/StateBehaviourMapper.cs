﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Utility/State Behaviour Mapper")]
public class StateBehaviourMapper : ScriptableObject
{
    [SerializeField]
    public List<StateBehaviourPair> StatetoBehaviour = new List<StateBehaviourPair>();

    public FlockBehaviour GetBehaviourForState(string stateName)
    {
        foreach(var statebehaviourpair in StatetoBehaviour)
        { 
            if(statebehaviourpair.Key() == stateName)
            {
                return statebehaviourpair.behaviour;
            }
        }

        return null;
    }
}
