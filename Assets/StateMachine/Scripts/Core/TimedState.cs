﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedState : State
{
    public State previousState;
    public float ActualWaitTimeStep = 3.0f;
    public float MinimumWaitTimeStep = 1.0f;
    public float MaximumWaitTimeStep = 5.0f;

    float CurrentWaitValue = 0.0f;

    void UpdateState(StateController controller)
    {

        CurrentWaitValue += Time.deltaTime;
        if(CurrentWaitValue> ActualWaitTimeStep)
        {
            DoActions(controller);
            CheckTransitions(controller);
            CurrentWaitValue = 0.0f;

        }
    }

}
