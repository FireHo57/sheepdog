﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PrePostStateFunction : ScriptableObject
{
    public abstract void BeforeState(StateController controller);

    public abstract void AfterState(StateController controller);
}
