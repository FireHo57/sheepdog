﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State")]
public class State : ScriptableObject
{
    public Action[] enterActions;
    public Action[] exitActions;
    public Action[] actions;
    public Transition[] transitions;

    public CompositeBehaviour behaviour;
    public Color stateColour;

    // controls how state is updated every frame
    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    //do all actions relevant to a specific state
    protected void DoActions(StateController controller)
    {
        if( actions.Length < 1 )
        {
            return;
        }

        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    public void OnEnter(StateController controller)
    {
        if( enterActions.Length > 0 )
        {
            foreach(Action action in enterActions)
            {
                action.Act(controller);
            }
        }
    }

    public void OnExit(StateController controller)
    {
        if (exitActions.Length > 0)
        {
            foreach (Action action in exitActions)
            {
                action.Act(controller);
            }
        }
    }

    protected void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSuceeded = transitions[i].decision.Decide(controller);
            if (decisionSuceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }

}
