﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateBehaviourPair 
{
    public State state;
    public FlockBehaviour behaviour;

    public string Key()
    {
        return state.name;
    }
}
