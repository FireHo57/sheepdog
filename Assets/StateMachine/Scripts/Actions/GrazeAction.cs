﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/GrazeAction")]
public class GrazeAction : Action
{
    public override void Act(StateController controller)
    {
        IEater eater = controller.GetComponentInParent<Sheeple>().EatingStrategy;
        Eatable eatable = controller.context[0].GetComponent<Eatable>();
        if (eater != null && eatable != null)
        {
            eater.Eat(controller.gameObject, eatable);
        }
    }
}
