﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Actions/MoveAction")]
public class MoveAction : Action
{
    public override void Act(StateController controller)
    {
        Move(controller);
    }

    private void Move(StateController controller)
    {
        FlockBehaviour behaviour = controller.currentBehaviour;
        Vector3 move = behaviour.CalculateMove(controller.agent, behaviour.GetContext(controller.agent), controller.agent.AgentFlock);
        controller.agent.Move(move*Time.deltaTime);
    }
}
