﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "PluggableAI/Actions/CalculatePath")]
public class CalculatePath : Action
{
    public override void Act(StateController controller)
    {
        // 3 states
        // no positions - there's no sensible thing we can do in this case, so just return
        // 1 position - this will be the seek target so generate a path for it
        // 1< positions - the path has already been generated, remove the first memeber in it and return

        int numberOfContexts = controller.context.Count;

        if( numberOfContexts == 0 )
        {
            return;
        }
        else if( numberOfContexts == 1 )
        {
            GenerateNextWaypoint(controller);
        }
        else
        {
            GameObject go = controller.context[0].gameObject;
            controller.context.RemoveAt(0);
            Destroy(go);
            GenerateNextWaypoint(controller);
        }

    }

    private void GenerateNextWaypoint(StateController controller)
    { 
        Vector3 agentPosition = controller.agent.transform.position;
        Vector3 targetPosition = controller.context[0].position;
        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(agentPosition, targetPosition, NavMesh.AllAreas, path);
        
        if(path.status != NavMeshPathStatus.PathComplete)
        {
            return;
        }

        // onyl care about the first waypoint
        GameObject go = new GameObject();
        go.transform.position = path.corners[1];
        go.name = "Waypoint";
        controller.context.Insert(0, go.transform);
    }

}
