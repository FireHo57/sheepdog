﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Navigator : MonoBehaviour
{
    /*
    // this is so I can compare floats
    float TOLERANCE = 0.001f;

    [SerializeField]
    Transform FollowTarget;

    Bounds MoveBoundsOverride;

    NavMeshAgent agent;
    bool runningAway = false;

    float startSpeed;
    float startRotationSpeed;

    enum MOVE_STATE
    {
        WANDERING,
        IN_PEN
    }

    MOVE_STATE state;

    // Start is called before the first frame update
    void Start()
    {
        state = MOVE_STATE.WANDERING;

        MoveBoundsOverride = new Bounds(new Vector3(0,0,0), new Vector3(-1,-1,-1));
        agent = GetComponent<NavMeshAgent>();
        startSpeed = agent.speed;
        startRotationSpeed = agent.angularSpeed;
        SetNewTarget();
    }

    bool BoundsCheck()
    {
        return !(MoveBoundsOverride.size.x < 0);
    }

    void SetNewTarget()
    {
        if( state == MOVE_STATE.WANDERING)
        {
            Vector2 generatedPos = Random.insideUnitCircle * currentRangeAndMove.move;
            Vector3 newTarget = new Vector3(generatedPos.x, 1.0f, generatedPos.y);
            agent.destination = transform.position + newTarget;
        }
        else if( state == MOVE_STATE.RETURNING )
        {
            Vector3 direction = transform.position - FollowTarget.position;
            direction.Normalize();
            float distance = Random.Range(0.0f, MoveBands[0].range);
            agent.destination = FollowTarget.position+ (distance*direction);
        }
        else if (state == MOVE_STATE.IN_PEN)
        {
            agent.destination = new Vector3(
                Random.Range(MoveBoundsOverride.min.x, MoveBoundsOverride.max.x),
                0.5f,
                Random.Range(MoveBoundsOverride.min.x, MoveBoundsOverride.max.z)
                );
        }
        
        else if (inGrass)
        {
            NewTargetRestricted(grass);
        }
        
    }

    void NewTargetWander()
    {

    }

    void NewTargetRestricted(Bounds bounds)
    {
        agent.destination = new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }
    



    // Update is called once per frame
    void Update()
    {
        if ((agent.destination - transform.position).magnitude < TOLERANCE)
        {
            if (runningAway)
            {
                runningAway = false;
                agent.speed = baseSpeed;
            }
            SetNewTarget();
        }
    }*/
}
