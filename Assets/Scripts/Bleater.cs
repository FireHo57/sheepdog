﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bleater : MonoBehaviour
{
    public List<AudioClip> bleats;
    public float minBleatCooldown;
    public float maxBleatCooldown;
    float bleatCooldown;
    float bleatCountDown = 0.0f;

    public List<AudioClip> alarmedBleats;

    AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        source = gameObject.AddComponent<AudioSource>();
        bleatCooldown = Random.Range(minBleatCooldown, maxBleatCooldown);
    }

    // Update is called once per frame
    void Update()
    {
        bleatCooldown -= Time.deltaTime;
        if (bleatCooldown < 0)
        {
            var bleat = bleats[Random.Range(0, bleats.Count)];
            source.PlayOneShot(bleat);
            bleatCooldown = Random.Range(minBleatCooldown, maxBleatCooldown) + bleat.length;
        }
    }

    public void AlarmedBleat()
    {
        source.PlayOneShot(alarmedBleats[Random.Range(0, alarmedBleats.Count)]);
    }
}
