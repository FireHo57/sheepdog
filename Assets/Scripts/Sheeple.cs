﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Sheeple : FlockAgent
{
    public GameObject stateController;

    private StateController m_stateController;
    public StateController M_StateController { get { return m_stateController; } }

    public FlockBehaviour overrideBehaviour;

    public IEater EatingStrategy;

    private void Start()
    {
        GameObject go = Instantiate(stateController, transform.position, transform.rotation, transform);
        m_stateController = go.GetComponent<StateController>();
        Initialise();
    }

    private void Update()
    {
        
        FlockBehaviour behaviour = m_stateController.currentBehaviour;
        // behaviour can be uninitialised at the start
        if (behaviour == null)
        {
            return;
        }

        List<Transform> context = behaviour.GetContext(this);

        if ( overrideBehaviour )
        {
            Debug.LogWarning("OVERRIDE BEHAVIOUR SET: "+overrideBehaviour);
            behaviour = overrideBehaviour;
            context = overrideBehaviour.GetContext(this);
        }
        
    }
    
}
