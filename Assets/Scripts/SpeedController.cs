﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Scriptables/SpeedController")]
public class SpeedController : ScriptableObject
{
    public float MaxSpeed;
    float MaxSpeedSquare;

    [SerializeField]
    [Range(0f, 100f)]
    float speed;


    public void Initialise()
    {
        MaxSpeedSquare = MaxSpeed * MaxSpeed;
    }

    public Vector3 AddSpeed(Vector3 move)
    {
        move *= speed;
        if (move.sqrMagnitude > MaxSpeedSquare)
        {
            move = move.normalized * MaxSpeed;
        }

        Debug.Log("Move: " + move);
        return move;
    }
}
