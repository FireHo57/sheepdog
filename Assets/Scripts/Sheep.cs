﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : MonoBehaviour
{
    /*
    enum STATE
    {
        WANDERING = 0,
        RETURNING = 1,
        GRAZING   = 2,
        RUNNING   = 3
    };

    [SerializeField]
    float range;

    [SerializeField]
    float tolerance;

    [SerializeField]
    float moveSpeed;
    [SerializeField]
    float runSpeed;
    [SerializeField]
    float runRotation;

    [SerializeField]
    float minRun;
       
    [SerializeField]
    float maxRun;

    [SerializeField]
    Transform followTarget;

    float baseSpeed;
    float baseRotation;

 

    Bounds pen;
    bool inPen = false;

    Bounds grass;
    bool inGrass = false;

    Flock parentFlock;

    LineRenderer lr;

    Bleater bleater;

    // Start is called before the first frame update
    void Start()
    {
        lr = gameObject.AddComponent<LineRenderer>();
        bleater = gameObject.GetComponent<Bleater>();
    }

    // Update is called once per frame
    void Update()
    {
        drawPath();

    }

    void drawPath()
    {
        if( agent.path.corners.Length < 2 )
        {
            return;
        }

        lr.positionCount = agent.path.corners.Length;
        lr.SetPositions(agent.path.corners);
    }



    private void OnDrawGizmos()
    {
        if(Application.isPlaying)
        {
            Gizmos.color = new Color(1, 0, 0, 0.5f);
            Gizmos.DrawCube(agent.destination, new Vector3(1, 1, 1));
            Gizmos.color = Color.blue;
        }
    }

    public void RunFrom(Vector3 awayFrom)
    {
        if (inPen)
        {
            return;
        }

        Vector3 toPosition = transform.position;
        toPosition.y = 0.0f;
        Vector3 fromPosition = awayFrom;
        fromPosition.y = 0.0f;
        Vector3 runVector = (toPosition - fromPosition).normalized;
        float runRange = Random.Range(minRun, maxRun);

        bleater.AlarmedBleat();

        RaycastHit hit;
        int layerMask = 1 << 8;
        if(Physics.Raycast(transform.position, runVector, out hit, runRange))
        {
            Vector3 hitPoint = hit.point;
            hitPoint.y = 0.0f;
            agent.destination = hitPoint;
        }
        else
        {
            agent.destination = transform.position + (runVector *= Random.Range(minRun, maxRun));
        }
        
        runningAway = true;
        agent.speed = runSpeed;
        agent.angularSpeed = runRotation;
    }

    public void InPen(Bounds reachedPen)
    {
        pen = reachedPen;
        inPen = true;
        agent.speed = 0.5f;
        agent.angularSpeed = 30;
        SetNewTarget();
    }

    public void InGrass(Bounds reachedGrass)
    {
        grass = reachedGrass;
        inGrass = true;
        agent.speed = 0.5f;
        agent.angularSpeed = 30;
        SetNewTarget();
    }

    public void LeftGrass()
    {
        inGrass = false;
    }*/
}
