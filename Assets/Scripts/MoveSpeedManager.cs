﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedManager : MonoBehaviour
{
    public float m_baseSpeed;
    public float m_runSpeed;

    struct RangeMoveSpeed
    {
        // if you're further away from this the range is not relevant
        float maxRange;

        // modifier
        float moveSpeed;
    }

    // serialize for debug purposes
    [SerializeField]
    List<RangeMoveSpeed> RangeAndMoveSpeedBands;

    // Start is called before the first frame update
    void Start()
    {
        RangeAndMoveSpeedBands = new List<RangeMoveSpeed>();
    }

    public void SetInitialSpeeds(float baseSpeed, float runSpeed)
    {
        m_baseSpeed = baseSpeed;
        m_runSpeed = runSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
