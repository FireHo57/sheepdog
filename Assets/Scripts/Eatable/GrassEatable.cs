﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassEatable : Eatable
{
    Vector3 startScale;

    void Start()
    {
        startScale = transform.localScale;
    }

    void Update()
    {
        if (b_amountEaten > 0.0f + 0.0001f)
        {
            // reduce scale compared to how much has been eaten
            float percentLeft = (1.0f - (b_amountEaten / b_amountToEat));
            transform.localScale = startScale * percentLeft;
        }
    }

    public override bool GetEaten(float value)
    {
        bool good = false;
        if (CanBeEaten())
        {
            b_amountEaten += value;
            good = true;
        }
        return good;
    }
}