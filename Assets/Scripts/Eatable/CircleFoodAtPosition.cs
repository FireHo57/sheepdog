﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="SheepleBehaviour/Eatables/CircleFoodAtPosition")]
public class CircleFoodAtPosition : FoodAtPositionFunction
{
    public override float FoodAtPosition(Vector3 position, Eatable eatable)
    {
        float foodLeft = (eatable.b_amountToEat - eatable.b_amountEaten);
        float distanceFromCenter = (eatable.transform.position - position).magnitude;
        //y = pi*x^3/3*foodleft + 3*foodLeft/pi*x^2
        // y is food at position, x is distance from center
        float r = eatable.transform.localScale.magnitude;
        float piRSquared = Mathf.PI * r * r;
        float threeFoodLeft = 3 * foodLeft;
        float a = threeFoodLeft / piRSquared;
        float foodAtPosition = -a * (1 / r) * distanceFromCenter + a;
        return foodAtPosition;
    }
}
