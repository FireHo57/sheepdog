﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SheepleBehaviour/Eaters/SheepleEater")]
public class SheepleEater : IEater
{
    // in units/second
    public float eatPerSecond = 10.0f;

    public override void Eat(GameObject eater, Eatable target)
    {
        float eatPerTimestep = eatPerSecond* Time.deltaTime;
        target.GetEaten(eatPerTimestep);
    }
}
