﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/*
[RequireComponent(typeof(NavMeshAgent))]
public class SheepNavigator : MonoBehaviour
{
    float _DELTA = 0.001f;
    NavMeshAgent _navigator;
    [SerializeField] float _MinWalkRange;
    [SerializeField] float _MaxWalkRange;


    [SerializeField] float _Speed;
    [SerializeField] float _RunSpeed;
    [SerializeField] float _RotationSpeed;
    [SerializeField] float _RunRotationSpeed;
    bool _isRunning = false;
    Bounds _RestrictedArea = new Bounds(new Vector3(0,0,0),new Vector3(-1,-1,-1));

    Flock _flock;

    // Start is called before the first frame update
    void Start()
    {
        _navigator = GetComponent<NavMeshAgent>();
        _navigator.speed = _Speed;
        _navigator.angularSpeed = _RotationSpeed;

        _flock = gameObject.GetComponent<Sheep>().GetFlock();
    }

    // Update is called once per frame
    void Update()
    {
        if(_navigator.remainingDistance < 0.0f + _DELTA)
        {
            if (_isRunning)
            {
                _isRunning = false;
                _navigator.speed = _Speed;
            }

            SetNewTarget();
        }
    }

    void SetNewTarget()
    {
        Vector2 direction = Random.insideUnitCircle.normalized;
        float range = Random.Range(_MinWalkRange, _MaxWalkRange);
        Vector2 displacement = direction * range;
    
        if (_RestrictedArea.size.x < 0.0f + _DELTA)
        {
            Vector3 flockPosition = _flock.transform.position;
            Vector3 diff3 = flockPosition - transform.position; 
            Vector2 diff2 = new Vector2(diff3.x,diff3.z);
            float dot = Vector2.Dot(direction.normalized, diff2.normalized);
            // opposite direction -1, same direction = 1
            Vector2 modifier = dot*_flock.GetElasticity() * diff2.normalized;
            displacement+=modifier;
            _navigator.destination = transform.position + new Vector3(displacement.x, 1.0f, displacement.y);
        }
        else
        {
            NewTargetRestricted(_RestrictedArea);
        }       
    }

    void NewTargetRestricted(Bounds bounds)
    {
        _navigator.destination = new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }
}*/