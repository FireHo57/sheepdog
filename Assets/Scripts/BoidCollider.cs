﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class BoidCollider : MonoBehaviour
{
    List<Transform> boids;

    [SerializeField]
    float scale;

    // Start is called before the first frame update
    void Start()
    {
        boids = new List<Transform>();
        transform.localScale = new Vector3(transform.localScale.x * scale, 1.0f, transform.localScale.z * scale);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject != transform.parent.gameObject && other.tag == "boid")
        {
            boids.Add(other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject != transform.parent.gameObject && other.tag == "boid")
        {
            boids.Remove(other.transform);
        }
    }

    public List<Transform> GetLocalBoids()
    {
        return boids;
    }
}
