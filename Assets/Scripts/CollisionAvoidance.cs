﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAvoidance : MonoBehaviour
{
    bool frontHit;
    bool leftHit;
    bool rightHit;

    [SerializeField]
    float RayLength;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 left = transform.forward;
        Vector3 right = transform.forward;
        left = Quaternion.Euler(0, -45, 0) * left;
        right = Quaternion.Euler(0, 45, 0) * right;

        frontHit = Physics.Raycast(transform.position, transform.forward, RayLength);
        leftHit = Physics.Raycast(transform.position, left, RayLength);
        rightHit = Physics.Raycast(transform.position, right, RayLength);

        Debug.Log("Left Front Right: " + leftHit + " " + frontHit + " " + rightHit);

//        Debug.DrawLine(transform.position, transform.position + transform.forward * RayLength, Color.green);
//        Debug.DrawLine(transform.position, transform.position + left * RayLength, Color.green);
//        Debug.DrawLine(transform.position, transform.position + right* RayLength, Color.green);
    }

    public bool GetFrontHit()
    {
        //Debug.Log("Front hit: " + frontHit);
        return frontHit;
    }

    public bool GetLeftHit()
    {
        //Debug.Log("Left hit: " + leftHit);
        return leftHit;
    }

    public bool GetRightHit()
    {
        //Debug.Log("Right hit: " + rightHit);
        return rightHit;
    }
}
