﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Geometry 
{
    // Quadratic formula -- x = [ -b(+-)sqrt(b^2 - 4ac) ]/2a

    public float[] QuadraticSolver(float a, float b, float c)
    {
        float[] results = new float[2];
        results[0] = (-b + Mathf.Sqrt(b * b - 4 * a * c)) / 2 * a;
        results[1] = (-b - Mathf.Sqrt(b * b - 4 * a * c)) / 2 * a;
        return results;
    }

    public class CircleEquation
    {
        // by default the 'equation' is stored in the format (x-w)^2 + (y-h)^2 = r^
        private float h;
        private float w;
        private float r_squared;

        //these values are calculated from the above equation, expanding the brackets -> x^2 + y^2 +Ax +By +c = 0
        private float A;
        private float B;
        private float C;

        public CircleEquation(Vector3 center, float radius)
        {
            h = center.y;
            w = center.x;
            r_squared = radius * radius;

            A = -2 * w;
            B = -2 * h;
            C = h * h + w * w - r_squared;
        }

        // using maths from https://mathworld.wolfram.com/Circle-LineIntersection.html
        public Vector3[] Intersect(Vector3 start, Vector3 end)
        {
            Vector3[] noIntersects = new Vector3[0];

            float dX = end.x - start.x;
            float dY = end.y - start.y;
            float dR = Mathf.Sqrt(dX * dX + dY * dY);
            float d_squaredR = dR * dR;
            float D = start.x * end.y - end.x * start.y;
            float D_squared = D * D;

            // check for intersections
            float delta = r_squared * d_squaredR - D_squared;

            if (delta <= 0 + 0.00001f)
            {
                // there is no intersection or the intersection is a tangent.
                return noIntersects;
            }
            else
            {
                float x1 = (D * dY + Mathf.Sign(dY) * dX * Mathf.Sqrt(r_squared * d_squaredR - D_squared)) / d_squaredR;
                float x2 = (D * dY - Mathf.Sign(dY) * dX * Mathf.Sqrt(r_squared * d_squaredR - D_squared)) / d_squaredR;

                float y1 = (-D * dY + Mathf.Sign(dY) * dX * Mathf.Sqrt(r_squared * d_squaredR - D_squared)) / d_squaredR;
                float y2 = (-D * dY - Mathf.Sign(dY) * dX * Mathf.Sqrt(r_squared * d_squaredR - D_squared)) / d_squaredR;

                Vector3[] intersects = new Vector3[2];
                intersects[0] = new Vector3(x1, 0, y1);
                intersects[1] = new Vector3(x2, 0, y2);
                return intersects;
            }
        }

        public bool IsPointInCircle(Vector3 point)
        {
            // convert to '2d'
            Vector3 twod_point = new Vector3(point.x, 0, point.z);
            float relativePositionSquared = (twod_point - new Vector3(w, 0, h)).sqrMagnitude;
            return relativePositionSquared <= (r_squared + 0.0001f);
        }

        public List<Vector3> BoundsWithinCircle(Bounds bounds)
        {
            List<Vector3> boundsList = new List<Vector3>();

            if( IsPointInCircle(bounds.max) )
            {
                boundsList.Add(bounds.max);
            }
            if( IsPointInCircle(bounds.min) )
            {
                boundsList.Add(bounds.min);
            }

            Vector3 topLeft = new Vector3(bounds.min.x, 0f, bounds.max.z);
            Vector3 bottomRight = new Vector3(bounds.max.x, 0f, bounds.min.y);

            if (IsPointInCircle(topLeft) )
            {
                boundsList.Add(topLeft);
            }
            if( IsPointInCircle(bottomRight) )
            {
                boundsList.Add(bottomRight);
            }

            return boundsList;
        }




    }


}
