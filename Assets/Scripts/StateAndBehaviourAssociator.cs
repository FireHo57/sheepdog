﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAndBehaviourAssociator : MonoBehaviour
{
    Dictionary<State, FlockBehaviour> Associator;

    private void Start()
    {
        Associator = new Dictionary<State, FlockBehaviour>();
    }

    public bool Associate(State state, FlockBehaviour behaviour)
    {
        if( Associator.ContainsKey(state) )
        {
            return false;
        }
        else
        {
            Associator.Add(state, behaviour);
            return true;
        }
    }

    public FlockBehaviour Get(State state)
    {
        return Associator[state];
    }

}
