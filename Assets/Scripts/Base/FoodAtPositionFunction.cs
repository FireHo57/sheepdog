﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FoodAtPositionFunction : ScriptableObject
{
    public abstract float FoodAtPosition(Vector3 position, Eatable eatable);
}
