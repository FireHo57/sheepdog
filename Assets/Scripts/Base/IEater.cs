﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IEater : ScriptableObject
{
    public float minimumFoodRequired=10;
    public abstract void Eat(GameObject eater, Eatable target);
}

