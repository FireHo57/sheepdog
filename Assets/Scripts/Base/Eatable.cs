﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Eatable : MonoBehaviour
{ 
    public float b_amountToEat;
    public float b_amountEaten;

    public FoodAtPositionFunction FoodAtPosition;

    public abstract bool GetEaten(float value);
    public bool CanBeEaten() => b_amountEaten < b_amountToEat+0.00001f;
    public bool EnoughToEat(GameObject eater, IEater strategy)
    {
        float foodAtPosition = FoodAtPosition.FoodAtPosition(eater.transform.position, this);
        return foodAtPosition >= strategy.minimumFoodRequired;
    }
}
