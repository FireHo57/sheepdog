﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Flock/Filter/FrontOfArcFilter")]
public class T_FrontOfArcFilter : ContextFilter
{
    public float forwardAngle=60f;
    public override List<Transform> Filter(FlockAgent agent, List<Transform> original)
    {
        List<Transform> filtered = new List<Transform>();
        foreach (Transform item in original)
        {
            // get angle between agent and object
            Vector3 toItem = item.position - agent.transform.position;
            toItem.Normalize();
            float angle = Vector3.Angle(agent.transform.forward, toItem);
            if( angle < forwardAngle/2 || angle > (360 - forwardAngle/2))
            {
                filtered.Add(item);
                Debug.DrawLine(agent.transform.position, item.position, Color.yellow);
            }
        }

        return filtered;
    }
}
