﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Actions/T_SetupAction")]
public class T_SetupAction : Action
{
    public override void Act(StateController controller)
    {
        if(controller.context.Count == 0)
        {
            var go = GameObject.FindGameObjectsWithTag("target")[0];
            controller.context.Add(go.transform);
        }
    }
}
