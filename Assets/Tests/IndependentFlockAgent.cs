﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndependentFlockAgent : FlockAgent
{
    [SerializeField] FlockBehaviour _behaviour;
    // Start is called before the first frame update
    void Start()
    {
        Initialise();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = _behaviour.CalculateMove(this, _behaviour.GetContext(this), AgentFlock);
        Move(move * Time.deltaTime);
    }
}
