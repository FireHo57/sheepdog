﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class FlockAgent : MonoBehaviour
{
    [Range(1f, 100f)]
    public float driveFactor = 10f;
    [Range(0, 100f)]
    public float maxSpeed = 5f;
    [Range(1f, 10f)]
    public float neighbourRadius = 20f;
    [Range(0f, 1f)]
    public float avoidanceRadiusMultiplier = 0.5f;

    float squareMaxSpeed;
    float squareNeighbourRadius;
    float squareAvoidanceRadius;
    public float SquareAvoidanceRadius { get { return squareAvoidanceRadius; } }

    public Vector3 currentVelocity = Vector3.zero;
    public Vector3 CurrentVelocity { get { return currentVelocity; } }
 

    Flock agentFlock;
    public Flock AgentFlock {  get { return agentFlock; } }

    Collider agentCollider;
    public Collider AgentCollider { get { return agentCollider; } }

    protected void Initialise()
    {
        squareMaxSpeed = maxSpeed * maxSpeed;
        squareNeighbourRadius = neighbourRadius * neighbourRadius;
        squareAvoidanceRadius = squareNeighbourRadius * avoidanceRadiusMultiplier * avoidanceRadiusMultiplier;
        agentCollider = GetComponent<Collider>();
    }

    public void Initialise(Flock flock)
    {
        agentFlock = flock;
    }

    public void Move(Vector3 velocity)
    {
        velocity = new Vector3(velocity.x, 0f, velocity.z);
        currentVelocity = velocity;
        float sqrCurrentVelocity = currentVelocity.magnitude * currentVelocity.magnitude;
        if(sqrCurrentVelocity> squareMaxSpeed)
        {
            Vector3 velocityNormalised = currentVelocity.normalized;
            currentVelocity = velocityNormalised * maxSpeed;
        }
        
        transform.position += currentVelocity;
        transform.rotation = Quaternion.LookRotation(currentVelocity);
    }
}
