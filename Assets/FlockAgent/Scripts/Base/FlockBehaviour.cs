﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FlockBehaviour : ScriptableObject
{
    // not all behaviours will use this however this provides a follow/flee target where applicable
    [HideInInspector] public Transform m_context = null;

    public abstract Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock);
    
    public virtual List<Transform> GetContext(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();
        Collider[] contextColliders = Physics.OverlapSphere(agent.transform.position, agent.neighbourRadius);
        foreach (Collider c in contextColliders)
        {
            if (c != agent.AgentCollider)
            {
                context.Add(c.transform);
            }
        }

        return context;
    }
}
