﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    public Sheeple sheeplePrefab;
    List<Sheeple> creatures = new List<Sheeple>();

    [Range(0, 100)]
    public int startingCount = 250;
    const float AgentSpacing = 0.5f;

    public RelativePositionGenerator PG;
    public SpeedController SC;

    public Transform target;


    StateAndBehaviourAssociator m_behaviourMap;

    void MapStatesToBehaviours()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        SC.Initialise();
        PG.SetRelativePosition(transform.position);

        Vector3[] positions = PG.GeneratePositions(startingCount, startingCount * AgentSpacing);
        for(int i = 0; i < startingCount; i++)
        { 
            Sheeple newAgent = Instantiate(
                sheeplePrefab,
                positions[i],
                Quaternion.Euler(Vector3.forward ),//* Random.Range(0, 360)),
                transform
                );
            newAgent.name = "Agent" + i;
            newAgent.Initialise(this);
            creatures.Add(newAgent);
        }

        //m_behaviourMap = new StateAndBehaviourAssociator();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        foreach(Sheeple sheep in creatures)
        {
            List<Transform> context = sheep.Behaviour.GetContext(sheep);
            FlockBehaviour behaviour = sheep.Behaviour;
            Vector3 move = behaviour.CalculateMove(sheep, context, this);
            move = SC.AddSpeed(move);
            sheep.Move(move);
            //FOR DEMO ONLY
            //agent.GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(Color.black, Color.white, context.Count / 6f);
        }
        */
    }

}
