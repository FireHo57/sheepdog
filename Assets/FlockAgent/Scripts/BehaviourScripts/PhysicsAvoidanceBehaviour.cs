﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Flock/Behaviour/PhysicsAvoidance")]
public class PhysicsAvoidanceBehaviour : FilteredFlockBehaviour
{
    // this isn't used
    Vector3 currentVelocity;

    public float agentSmoothTime = 0.5f;

    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (context.Count == 0)
        {
            return Vector3.zero;
        }

        Vector3 avoidanceMove = Vector3.zero;
        int nAvoid = 0;
        List<Transform> filteredContext = Filter(agent, context);

        if (filteredContext.Count == 0)
        {
            return Vector3.zero;
        }

        foreach (Transform item in filteredContext)
        {
            Collider itemCollider = item.GetComponent<Collider>();

            if (itemCollider == null)
            {
                return Vector3.zero;
            }

            Bounds colliderBounds = itemCollider.bounds;
            Vector3 closestPoint = colliderBounds.ClosestPoint(agent.transform.position);
           
            if (Vector3.SqrMagnitude(closestPoint - agent.transform.position) < agent.SquareAvoidanceRadius)
            {
                nAvoid++;
                //away from the closest point
                Vector3 direction = (agent.transform.position - closestPoint).normalized;
                Debug.DrawLine(agent.transform.position, closestPoint, Color.green);
                //scale by how close to the closest point we are
                float scale = agent.SquareAvoidanceRadius - (agent.transform.position - closestPoint).sqrMagnitude;

                avoidanceMove += scale*direction;
            }

        }

        if (nAvoid > 0)
        {
            avoidanceMove /= nAvoid;
        }

        avoidanceMove = Vector3.SmoothDamp(agent.transform.forward, avoidanceMove, ref currentVelocity, agentSmoothTime);
        return avoidanceMove;
    }

    List<Vector3> GetSortedBoundPositions(Bounds bounds, FlockAgent agent)
    {
        List<Vector3> boundsInList = new List<Vector3>();
        boundsInList.Add(bounds.max);
        boundsInList.Add(bounds.min);
        boundsInList.Add(new Vector3(bounds.max.x, 0f, bounds.min.y));
        boundsInList.Add(new Vector3(bounds.min.x, 0f, bounds.max.y));


        // Get the two closest bounds
        var nearest = boundsInList
            .OrderBy(t => Vector3.Distance(agent.transform.position, t))
            .FirstOrDefault();

        return boundsInList;
    }

    Vector3 GetSteerNoBounds(Geometry.CircleEquation circle, Bounds bounds, FlockAgent agent)
    {
        List<Vector3> sortedBoundPositions = GetSortedBoundPositions(bounds, agent);
        Vector3[] Intersections = circle.Intersect(sortedBoundPositions[0], sortedBoundPositions[1]);
        return (agent.transform.position - Intersections[0]) + (agent.transform.position - Intersections[1]);
    }

    Vector3 GetSteerOneBound(Geometry.CircleEquation circle, Bounds bounds, FlockAgent agent)
    {
        List<Vector3> sortedBoundPositions = GetSortedBoundPositions(bounds, agent);
        Vector3[] Intersection1 = circle.Intersect(sortedBoundPositions[0], sortedBoundPositions[1]);
        Vector3[] Intersection2 = circle.Intersect(sortedBoundPositions[0], sortedBoundPositions[2]);
        return sortedBoundPositions.Aggregate(new Vector3(0, 0, 0), (s, v) => s + v);
    }


}
