﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActivatableFilteredFlockBehaviours : FilteredFlockBehaviour 
{
    private bool i_isActive = false;

    public bool I_isActive { get => I_isActive1; set => I_isActive1 = value; }
    public bool I_isActive1 { get => i_isActive; set => i_isActive = value; }
}
