﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Flock/Behaviour/Alignment")]
public class AlignmentBehaviour : FilteredFlockBehaviour
{
    Vector3 currentVelocity;
    public float agentSmoothTime = 0.5f;

    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if(context.Count == 0)
        {
            return agent.transform.forward;
        }

        Vector3 alignmentMove = Vector3.zero;

        List<Transform> filteredContext = Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            alignmentMove += item.forward;
        }

        alignmentMove /= context.Count;

        return alignmentMove;
    }

    
}
