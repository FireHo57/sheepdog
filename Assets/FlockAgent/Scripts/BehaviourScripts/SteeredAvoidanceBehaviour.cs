﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaviour/SteeredAvoidance")]
public class SteeredAvoidanceBehaviour : FilteredFlockBehaviour
{
    Vector3 currentVelocity;
    [SerializeField] float agentSmoothTime = 0.5f;

    [SerializeField] float distanceWeight = 5.0f;
    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (context.Count == 0)
        {
            return Vector3.zero;
        }

        Vector3 avoidanceMove = Vector3.zero;
        int nAvoid = 0;
        List<Transform> filteredContext = Filter(agent, context);
        Vector3 agentPosition = agent.transform.position;
        foreach (Transform item in filteredContext)
        {
            // what is the nearest point of the collider
            Vector3 collisionPosition = item.gameObject.GetComponent<Collider>().ClosestPoint(agentPosition);
            Vector3 collisionDirection = collisionPosition - agentPosition;
            float collisionDistanceSqr = collisionDirection.sqrMagnitude;
            collisionDirection.Normalize();

            if( collisionDistanceSqr < agent.SquareAvoidanceRadius)
            {
                // care about 2 things how close, and how left/right of forward
                float distanceInsideAvoidance = agent.SquareAvoidanceRadius - collisionDistanceSqr;
                float distanceMultiplier = distanceInsideAvoidance / agent.SquareAvoidanceRadius;
                distanceMultiplier *= distanceMultiplier;
                float collisionAngle = Vector3.Angle(agent.transform.forward, collisionDirection);

                if(collisionAngle < 90 || collisionAngle > 270)
                {
                    nAvoid++;
                    bool turnRight = false;
                    if (collisionAngle > 90)
                    {
                        turnRight = true;
                        collisionAngle -= 270;
                    }

                    float swerveAngle = 90 - collisionAngle;
                    if(!turnRight)
                    {
                        swerveAngle = -swerveAngle;
                    }
                    Debug.Log(swerveAngle);
                    Vector3 swerveDirection = Quaternion.Euler(0, swerveAngle, 0) * agent.transform.forward;

                    // modify the swerve vector depending on how close to the collision we are
                    Vector3 forwardBackModifier = agent.transform.forward*Mathf.Lerp(-1f, 1f, collisionDistanceSqr / agent.SquareAvoidanceRadius);
                    avoidanceMove += ((distanceMultiplier * distanceWeight) * swerveDirection )+ forwardBackModifier;
                }
            }
        }

        if (nAvoid > 0)
        {
            avoidanceMove /= nAvoid;
        }

        //Debug.DrawLine(agentPosition, agentPosition + avoidanceMove, Color.red);
        Vector3 adjustedAvoidanceMove = Vector3.SmoothDamp(agent.transform.forward, avoidanceMove, ref currentVelocity, agentSmoothTime);
        Debug.DrawLine(agent.transform.position, agent.transform.position + adjustedAvoidanceMove, Color.red);
        return adjustedAvoidanceMove;
    }

}