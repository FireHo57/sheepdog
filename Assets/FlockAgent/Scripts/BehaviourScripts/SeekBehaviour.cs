﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Flock/Behaviour/SeekBehaviour")]
public class SeekBehaviour : FlockBehaviour
{
    public Vector3 overrideContext = new Vector3(-1, -1, -1);

    Vector3 currentVelocity;
    float agentSmoothTime = 0.5f;

    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        // calculate the distance between the boid and the target
        Sheeple sheeple = (Sheeple)agent;
        Vector3 contextPosition = new Vector3();
        if( !overrideContext.Equals(new Vector3(-1,-1,-1) ) )
        {
            contextPosition = overrideContext;
        }
        else
        {
            contextPosition = sheeple.M_StateController.context[0].position;
        }

        Debug.DrawLine(agent.transform.position, contextPosition);
        Vector3 followDirection = (contextPosition - agent.transform.position).normalized;

        Vector3 followMove = Vector3.SmoothDamp(agent.transform.forward, followDirection, ref currentVelocity, agentSmoothTime);
        return followMove;
    }

    public override List<Transform> GetContext(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();
        context.Add(m_context);

        return context;
    }

}
