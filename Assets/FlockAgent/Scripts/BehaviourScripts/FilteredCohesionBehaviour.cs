﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilteredCohesionBehaviour : FilteredFlockBehaviour
{
    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (context.Count < 1)
        {
            return Vector3.zero;
        }

        List<Transform> filteredContext = Filter(agent, context); 

        Vector3 cohesionMove = Vector3.zero;
        foreach (Transform item in filteredContext)
        {
            cohesionMove += item.position;
        }

        cohesionMove /= filteredContext.Count;
        cohesionMove -= agent.transform.position;
        return cohesionMove;
    }
}
