﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FilteredFlockBehaviour : FlockBehaviour
{
    public ContextFilter[] filters;

    public List<Transform> Filter(FlockAgent agent, List<Transform> context)
    {
        List<Transform> filteredContext = context;
        foreach(ContextFilter f in filters)
        {
            filteredContext = f.Filter(agent, filteredContext);
        }

        return filteredContext;
    }
}
