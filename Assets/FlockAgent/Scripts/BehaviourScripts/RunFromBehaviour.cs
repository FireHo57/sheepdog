﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Flock/Behaviour/RunFromBehaviour")]
public class RunFromBehaviour : ActivatableFilteredFlockBehaviours
{
    public Transform target;
    public float RunSpeed;

    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        if (!I_isActive || target == null)
        {
            return Vector3.zero;
        }

        Vector3 direction = agent.transform.position - target.position;
        float distanceScalar = RunSpeed - (direction.magnitude * direction.magnitude);
        if (distanceScalar < 0 + 0.001f)
        {
            distanceScalar = 0f;
        }

        return distanceScalar * (direction.normalized);
    }

}
