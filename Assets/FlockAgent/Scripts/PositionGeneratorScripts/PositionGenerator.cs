﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PositionGenerator : ScriptableObject
{
    public abstract Vector3[] GeneratePositions(int number, float spacing);
}
