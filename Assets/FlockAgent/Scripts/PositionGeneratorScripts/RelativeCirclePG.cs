﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Flock/Position Generator/Relative Circle Position Generator")]
public class RelativeCirclePG : RelativePositionGenerator
{
    private Vector3 m_relativePosition;

    public override Vector3[] GeneratePositions(int number, float spacing)
    {
        if(m_relativePosition == null)
        {
            Debug.Log("Relative Transform not set in " + this);
            return new Vector3[0];
        }

        Vector3[] positions = new Vector3[number];

        for (int i = 0; i < number; i++)
        {
            Vector2 two_d_pos = Random.insideUnitCircle * spacing;
            Vector3 pos = new Vector3(two_d_pos.x, 1, two_d_pos.y);
            pos += m_relativePosition;
            positions[i] = pos;
        }

        return positions;
    }

    public override void SetRelativePosition(Vector3 position)
    {
        m_relativePosition = position;
    }
}
