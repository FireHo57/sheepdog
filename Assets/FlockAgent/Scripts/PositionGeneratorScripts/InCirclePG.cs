﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Flock/Position Generator/In Circle Position")]
public class InCirclePG : PositionGenerator
{
    public override Vector3[] GeneratePositions(int number, float spacing)
    {
        Vector3[] positions = new Vector3[number];

        for(int i = 0; i < number; i++)
        {
            Vector2 two_d_pos = Random.insideUnitCircle * spacing;
            Vector3 pos = new Vector3(two_d_pos.x, 1, two_d_pos.y);
            positions[i] = pos;
        }

        return positions;
    }
}
