﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RelativePositionGenerator : PositionGenerator
{
    // sets th center position that the positions will be generated relative to
    public abstract void SetRelativePosition(Vector3 position);
}
