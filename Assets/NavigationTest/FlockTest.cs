﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockTest : MonoBehaviour
{
    [SerializeField]
    Transform target;

    [SerializeField]
    List<Transform> boids;

    Camera main;

    float startY;
    // Start is called before the first frame update
    void Start()
    {
        startY = transform.position.y;
        main = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 centrePoisition = new Vector3(0,0,0);
        foreach(Transform boid in boids)
        {
            centrePoisition += boid.position;
        }

        transform.position = new Vector3(centrePoisition.x, startY, centrePoisition.z);
        
    }

    public Vector3 GetTarget()
    {
        return target.position;
    }
}
