﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BoidTest : MonoBehaviour
{
    [SerializeField]
    Transform target;

    [SerializeField]
    float m_speedMultiplier;

    [SerializeField]
    float rotationSpeed;

    [SerializeField]
    float SwerveSpeed;

    BoidCollider m_collider;

    STATE currentState =  STATE.WANDER;

    [SerializeField]
    float minRunTime;
    [SerializeField]
    float maxRunTime;


    float RunTime;

    enum STATE
    {
        WANDER = 0,
        FLEE = 1,
        FOLLOW = 2
    }

    [System.Serializable]
    class TunableValues
    {
        public float FleeFrom;
        public float StayTogethor;
        public float Cohesion;
        public float Avoid;
        public float Swerve;
    }

    [SerializeField]
    TunableValues initial;

    [SerializeField]
    TunableValues flee;

    [SerializeField]
    TunableValues follow;

    [SerializeField]
    float MaxSteer;

    [SerializeField]
    float maxSpeed;
    Vector3 CurrentVelocity;

    // Start is called before the first frame update
    void Start()
    {
        m_collider = GetComponentInChildren<BoidCollider>();
        RunTime=0f;
        CurrentVelocity = new Vector3(0,0,0);

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 steering = new Vector3(0, 0, 0);
        steering += SeparationSteer();
        steering += CoherenceSteer();
        //steering += FollowSteer();



        steering.Normalize();

        Vector3 VelocityChange = (transform.position + steering*maxSpeed) + (transform.position + CurrentVelocity);

        CurrentVelocity += VelocityChange;
        CurrentVelocity.y = 0f;

        Debug.Log("VelocityChange: " + VelocityChange);
        Debug.Log("CurrentVelocity: " + CurrentVelocity);

        transform.position = transform.position + CurrentVelocity*Time.deltaTime;
        transform.rotation = Quaternion.LookRotation(CurrentVelocity);
    }

    Vector3 SeparationSteer()
    { 
        List<Transform> boids = m_collider.GetLocalBoids();

        Vector3 closestPosition = new Vector3(0,0,0);
        float closestValue = 100f;
        foreach (Transform boid in boids)
        {
            Vector3 relative = transform.position - boid.position;
            if (relative.magnitude < closestValue)
            {
                closestValue = relative.magnitude;
                closestPosition = boid.position;
            }
        }

        Debug.DrawLine(transform.position, closestPosition, Color.green);
        return closestPosition;
    }

    Vector3 CoherenceSteer()
    {
        Vector3 collectedPositions = transform.position;

        List<Transform> boids = m_collider.GetLocalBoids();

        foreach (Transform boid in boids)
        {
            collectedPositions += boid.position;
        }

        collectedPositions /= (boids.Count + 1);
        Debug.DrawLine(transform.position, collectedPositions, Color.blue);
        return (collectedPositions - transform.position).normalized;
    }


    Vector3 FollowSteer()
    {
        Vector3 desiredVelocity = (target.position - transform.position).normalized*maxSpeed;
        return desiredVelocity - CurrentVelocity;
    }



    void Steer()
    {
        TunableValues current;
        if(currentState == STATE.FLEE)
        {
            current = flee;
        }
        else if(currentState == STATE.FOLLOW)
        {
            current = follow;
        }
        else
        {
            current = initial;
        }

        Vector3 steer = new Vector3(0, 0, 0);
        // head towards a target
        if (currentState == STATE.FLEE)
        {
            Vector3 awayFromTarget = transform.position - target.position;

            awayFromTarget.Normalize();

            steer += awayFromTarget * current.FleeFrom;

            RunTime -= Time.deltaTime;
            if (RunTime + 0.001f < 0)
            {
                currentState = STATE.WANDER;
            }
        }
        else if (currentState == STATE.FOLLOW)
        {
            Vector3 towardtarget = target.position - transform.position;
            towardtarget.Normalize();

            steer += towardtarget * 1;
        }

        Vector3 swerve = new Vector3(0, 0, 0);

        RaycastHit hit;

        if(Physics.Raycast(transform.position,transform.forward,out hit))
        {
            float hitDistance = (hit.point - transform.position).magnitude;
            swerve = transform.right*(current.Swerve / hitDistance);
            Debug.Log(swerve);
        }


        Vector3 collectedPositions = transform.position;
        Vector3 collectedForwards = transform.forward;

        List<Transform> boids = m_collider.GetLocalBoids();

        float closestValue = 100f;
        Vector3 closestPosition = new Vector3(0, 0, 0);

        foreach (Transform boid in boids)
        {
            collectedPositions += boid.position;
            collectedForwards += boid.forward;
            Vector3 relativePosition = boid.position - transform.position;
            if( relativePosition.magnitude < closestValue)
            {
                closestValue = relativePosition.magnitude;
                closestPosition = boid.position;
            }
        }

        collectedPositions /= boids.Count+1;
        collectedForwards /= boids.Count + 1;
        //Debug.DrawLine(transform.position, collectedPositions, Color.blue);
        //Debug.DrawLine(transform.position, transform.position + collectedForwards, Color.green);
        //Debug.DrawLine(transform.position, closestPosition, Color.cyan);

        Vector3 centreSteer = collectedPositions - transform.position;

        Vector3 avoidanceSteer = transform.position - closestPosition;

        steer += centreSteer * current.StayTogethor;
        steer += avoidanceSteer * current.Avoid;
        steer += collectedForwards * current.Cohesion;
        //steer += swerve;

        steer.Normalize();

        Debug.DrawLine(transform.position, transform.position + steer);

        Quaternion targetRotation = Quaternion.Slerp(Quaternion.LookRotation(transform.forward), Quaternion.LookRotation(steer), Time.deltaTime * rotationSpeed);

        targetRotation.eulerAngles = new Vector3(0f, targetRotation.eulerAngles.y, 0f);

        transform.rotation = targetRotation;
        float speed = 0f;
        if(currentState == STATE.WANDER)
        {
            speed = Random.Range(0f, 1f);
        }
        else if( currentState == STATE.FLEE)
        {
            speed = Random.Range(2f, 5f);
        }
        else if( currentState == STATE.FOLLOW)
        {
            speed = Random.Range(2f, 4f);
        }


        transform.position += transform.forward * speed * Time.deltaTime;

        if(Input.GetMouseButtonDown(0))
        {
            RunAway();
        }
        else if( Input.GetMouseButtonDown(1))
        {
            Follow();
        }

    }
    
    public void RunAway()
    {
        RunTime = Random.Range(minRunTime, maxRunTime);
        currentState = STATE.FLEE;
    }

    public void Follow()
    {
        if (currentState == STATE.FOLLOW)
        {
            currentState = STATE.WANDER;
        }
        else
        {
            currentState = STATE.FOLLOW;
        }
    }

}
